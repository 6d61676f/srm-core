use std::{
    collections::hash_map::DefaultHasher,
    hash::{Hash, Hasher},
    path::{Path, PathBuf},
    time::SystemTime,
};

pub mod error;

use error::Result;

use path_absolutize::Absolutize;

pub type Handle = u64;

#[derive(Debug, Serialize, Deserialize, Hash, PartialEq, Eq, Clone)]
pub struct Id {
    pub del_time: SystemTime,
    pub orig_path: PathBuf,
}

impl Id {
    pub fn get_handle(&self) -> Handle {
        let mut hasher = DefaultHasher::new();
        self.hash(&mut hasher);
        hasher.finish()
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Meta {
    pub id: Id,
    pub back_path: PathBuf,

    // of calling process
    pub pid: usize,
}

impl Meta {
    pub fn load<P: AsRef<Path>>(p: P) -> Result<Vec<Meta>> {
        Ok(serde_json::from_str(&std::fs::read_to_string(p)?)?)
    }

    pub fn unload(metas: &[&Meta]) -> Result<String> {
        Ok(serde_json::to_string(metas)?)
    }

    pub fn create_for<P: AsRef<Path>>(p: P) -> Result<Meta> {
        Ok(Meta {
            id: Id {
                del_time: SystemTime::now(),
                orig_path: p.as_ref().absolutize()?.to_path_buf(),
            },
            back_path: PathBuf::new(),
            pid: std::process::id() as usize,
        })
    }

    pub fn get_handle(&self) -> Handle {
        self.id.get_handle()
    }
}
