use super::{
    error::{Error, Result},
    BackHandle, Removal,
};
use crate::{
    config::{self, Cfg, Config},
    metadata::{Handle, Id, Meta},
    utils::{
        self,
        fs::{FileLock, LockHandle, Mount},
    },
};
use globset::{Glob, GlobSet, GlobSetBuilder};
use path_absolutize::Absolutize;
use rand::RngCore;
use std::{
    collections::{
        hash_map::{DefaultHasher, Values},
        HashMap,
    },
    hash::{Hash, Hasher},
    path::{Path, PathBuf},
    rc::Rc,
    thread,
    time::Duration,
};

type Rcp = Rc<PathBuf>;
type Rcm = Rc<Meta>;

pub struct RemovalImpl {
    cfg: Cfg,
    handle_to_mpath: HashMap<BackHandle, Rcp>,
    mpath_to_meta: HashMap<Rcp, HashMap<BackHandle, Rcm>>, // meta path -> file metadatas
    mnt_utils: Mount,
    glob_set: Option<GlobSet>,
    id: String,
}

impl RemovalImpl {
    pub fn new() -> RemovalImpl {
        let mut mnt = Mount::new();
        mnt.refresh();

        let id = format!("{}-{}", std::process::id(), rand::thread_rng().next_u32());

        info!("created removal backend with id {}", id);

        let mut rmv = RemovalImpl {
            cfg: config::get_config(),
            handle_to_mpath: HashMap::new(),
            mpath_to_meta: HashMap::new(),
            mnt_utils: mnt,
            glob_set: None,
            id,
        };

        rmv.build_globset();
        rmv.reload_all_cfg();

        rmv
    }

    fn build_globset(&mut self) {
        let mut builder = GlobSetBuilder::new();

        self.cfg.excl_paths().iter().for_each(|p| {
            if let Ok(glob) = Glob::new(p.as_str()) {
                builder.add(glob);
            } else {
                warn!("{} is not a valid glob", p.as_str());
            }
        });

        if let Ok(globset) = builder.build() {
            self.glob_set = Some(globset);
        } else {
            error!("could not build globset");
        }
    }

    #[cfg(test)]
    pub fn new_custom_cfg<P: AsRef<Path>>(p: P) -> RemovalImpl {
        let mut rmv = RemovalImpl::new();
        rmv.cfg = config::Cfg::new_custom(p);
        rmv.build_globset();

        rmv
    }

    fn erase_single_id(&mut self, id: BackHandle) -> Result<()> {
        debug!("erasing entry for handle {}", id);

        let (lock, m_path, metas, meta) = self.get_synced_meta(id)?;

        debug!("removing backup for {:?} {:?}", id, meta);
        utils::fs::rm(&meta.back_path)?;

        // update meta
        metas.remove(&id);

        if metas.is_empty() {
            drop(lock);
            let meta_parent = m_path.parent().ok_or(Error::Missing)?;
            debug!(
                "cleaning backup dir {:?} after erase of {}",
                meta_parent, id
            );
            utils::fs::rm(meta_parent)?;
        } else {
            let updated_metas = RemovalImpl::serialize_metas(metas.values())?;
            RemovalImpl::write_cfg(self.id.as_bytes(), &*m_path, updated_metas.as_bytes())?;
        }
        self.handle_to_mpath.remove(&id);

        debug!("erased {}", id);

        Ok(())
    }

    fn restore_single_id(&mut self, id: BackHandle) -> Result<()> {
        debug!("restore entry for handle {}", id);

        let (lock, m_path, metas, meta) = self.get_synced_meta(id)?;

        debug!(
            "moving backup({:?}) to orig({:?})",
            meta.back_path, meta.id.orig_path
        );

        if meta.id.orig_path.exists() {
            warn!("restore path already exists, skipping...");
            return Err(Error::Exists);
        }

        // TODO -- theoretically if these two fail we should revert the move....
        utils::fs::mv(&meta.back_path, &meta.id.orig_path)?;

        metas.remove(&id);

        if metas.is_empty() {
            drop(lock);
            let meta_parent = m_path.parent().ok_or(Error::Missing)?;
            debug!(
                "cleaning backup dir {:?} after restore of {}",
                meta_parent, id
            );
            utils::fs::rm(meta_parent)?;
        } else {
            let updated_metas = RemovalImpl::serialize_metas(metas.values())?;
            RemovalImpl::write_cfg(self.id.as_bytes(), &*m_path, updated_metas.as_bytes())?;
        }

        self.handle_to_mpath.remove(&id);

        debug!("restored {}", id);

        Ok(())
    }

    fn backup_single_path<P: AsRef<Path>>(&mut self, p: P) -> Result<BackHandle> {
        debug!("backing up file {:?}", p.as_ref());

        let p = RemovalImpl::sanitize_path(&p)?;

        if self.handle_excluded(&p)? {
            return Err(Error::Excluded);
        }

        // get meta location
        let meta_path = self.cfg.meta_path(&p)?;

        let mut _lock = RemovalImpl::lock(meta_path.as_ref())?;
        self.reload_cfg(&*meta_path)?;

        // create meta for path
        let meta = self.gen_meta(&p)?;
        let meta_handle = meta.get_handle();
        debug!("handle {:?} has meta {:?}", meta_handle, meta);

        let metas = self
            .mpath_to_meta
            .get_mut(&meta_path)
            .ok_or(Error::Missing)?;

        let original_metas = RemovalImpl::serialize_metas(metas.values())?;

        metas.insert(meta_handle, meta.clone());
        self.handle_to_mpath.insert(meta_handle, meta_path.clone());

        let updated_metas = RemovalImpl::serialize_metas(metas.values())?;
        RemovalImpl::write_cfg(self.id.as_bytes(), &*meta_path, updated_metas.as_bytes())?;

        //METADATA HAS BEEN WRITTEN AT THIS POINT WE NEED TO REVERT ON ERRORS
        if let Err(e) = utils::fs::mv(&p, &meta.back_path) {
            error!("Move operation has failed ({:?}).reverting metadata", e);
            RemovalImpl::write_cfg(self.id.as_bytes(), &*meta_path, original_metas.as_bytes())?;
            metas.remove(&meta_handle);
            self.handle_to_mpath.remove(&meta_handle);
            Err(Error::Utils(e))
        } else {
            debug!(
                "{} backed up to {}, handle({})",
                p.to_string_lossy(),
                meta.back_path.to_string_lossy(),
                meta_handle
            );
            Ok(meta_handle)
        }
    }

    fn lock<P: AsRef<Path>>(path: P) -> Result<LockHandle> {
        let mut path = path.as_ref().to_path_buf();
        path.set_extension("lock");

        let fl = FileLock::try_new(&path)?;
        for _ in 0..100 {
            match fl.try_lock() {
                Err(utils::error::Error::Io((std::io::ErrorKind::AlreadyExists, _))) => {
                    thread::sleep(Duration::from_millis(
                        150 + rand::thread_rng().next_u64() % 150,
                    ));
                }

                Err(e) => {
                    error!(
                        "could not lock {}, received {:?}",
                        path.to_string_lossy(),
                        e
                    );
                    return Err(e.into());
                }

                Ok(val) => {
                    return Ok(val);
                }
            }
        }

        Err(Error::TimedOut)
    }

    fn write_cfg<P: AsRef<Path>>(id: &[u8], p: P, buf: &[u8]) -> Result<()> {
        std::fs::write(&p, buf).map_err(|err| {
            error!("could not write cfg to {}", p.as_ref().to_string_lossy());
            err
        })?;

        let mut idp = p.as_ref().to_path_buf();
        idp.set_extension("id");

        std::fs::write(&idp, id).map_err(|err| {
            error!("could not write id to {}", idp.to_string_lossy());
            err
        })?;

        Ok(())
    }

    fn get_synced_meta(
        &mut self,
        id: Handle,
    ) -> Result<(LockHandle, Rcp, &mut HashMap<Handle, Rcm>, Rcm)> {
        let path = self.get_meta_path_from_handle(id)?;
        let lock = RemovalImpl::lock(&*path)?;
        self.reload_cfg(&*path)?;

        let metas = self.mpath_to_meta.get_mut(&path).ok_or_else(|| {
            error!(
                "could not find meta entry for path({:?}), id({:?})",
                path, id
            );
            Error::Missing
        })?;

        let meta = metas.get(&id).cloned().ok_or_else(|| {
            warn!("no backup file has handle {}", id);
            Error::Missing
        })?;

        Ok((lock, path, metas, meta))
    }

    fn reload_cfg<P: AsRef<Path>>(&mut self, p: P) -> Result<()> {
        let mut idp = p.as_ref().to_path_buf();
        idp.set_extension("id");

        // try to read id
        if let Ok(id) = std::fs::read_to_string(&idp) {
            if id == self.id {
                return Ok(());
            }
        }

        let mut handles = HashMap::new();
        let mut fm = HashMap::new();
        let path = Rc::new(p.as_ref().to_path_buf());

        if let Ok(meta) = Meta::load(&p) {
            meta.into_iter().for_each(|m| {
                let hash = m.get_handle();
                fm.insert(hash, Rc::new(m));
                handles.insert(hash, path.clone());
            });
        }

        self.mpath_to_meta.insert(path, fm);
        self.handle_to_mpath.extend(handles);

        Ok(())
    }

    fn reload_all_cfg(&mut self) {
        self.cfg.get_all_meta_paths().into_iter().for_each(|p| {
            self.reload_cfg(p.as_ref())
                .unwrap_or_else(|err| warn!("could not load {:?} {:?}", &p, err))
        });
    }

    fn sanitize_path<P: AsRef<Path>>(p: P) -> Result<PathBuf> {
        Ok(p.as_ref().absolutize()?.to_path_buf())
    }

    fn gen_meta<P: AsRef<Path>>(&mut self, path: P) -> Result<Rcm> {
        let mut meta = Meta::create_for(path)?;
        // we need to determine backup path
        // store backups in format srm_home/hash(orig_file_path_hash)/hash(orig_file_path_hash)
        meta.back_path = self.cfg.back_path(&meta.id.orig_path)?.to_path_buf();
        meta.back_path = meta
            .back_path
            .join(RemovalImpl::calc_hash(&meta.id.orig_path).to_string());

        //check if backups already exist and maximum count
        meta.back_path = RemovalImpl::increment_backup_count(meta.back_path, self.cfg.count())?;

        Ok(meta.into())
    }

    fn serialize_metas<T>(metas: Values<'_, T, Rcm>) -> Result<String> {
        Ok(Meta::unload(
            &metas.map(|val| val.as_ref()).collect::<Vec<&Meta>>(),
        )?)
    }

    fn increment_backup_count(path: PathBuf, max_backup: usize) -> Result<PathBuf> {
        if path.symlink_metadata().is_err() {
            Ok(path)
        } else {
            let orig_name = path
                .file_name()
                .ok_or(Error::Missing)?
                .to_str()
                .ok_or(Error::Missing)?;

            let orig_parent = path.parent().ok_or(Error::Missing)?;

            for i in 0..max_backup {
                let new_path = orig_parent.join(format!("{}.{}", orig_name, i));
                if new_path.symlink_metadata().is_err() {
                    return Ok(new_path);
                }
            }
            Err(Error::Invalid)
        }
    }

    fn get_meta_path_from_handle(&self, id: BackHandle) -> Result<Rcp> {
        self.handle_to_mpath.get(&id).cloned().ok_or(Error::Missing)
    }

    fn handle_excluded<P: AsRef<Path>>(&self, p: P) -> Result<bool> {
        Ok(self.handle_tmpfs(&p)? || self.handle_specialfs(&p)? || self.handle_globbbed(&p)?)
    }

    fn handle_globbbed<P: AsRef<Path>>(&self, path: P) -> Result<bool> {
        let path = path.as_ref();

        if let Some(matcher) = &self.glob_set {
            if matcher.is_match(path) {
                return Ok(utils::fs::rm(path).map(|_| true)?);
            }
        }

        Ok(false)
    }

    fn handle_tmpfs<P: AsRef<Path>>(&self, p: P) -> Result<bool> {
        let p = p.as_ref();
        if self.cfg.exclude_tmpfs() && self.mnt_utils.is_tmpfs(p)? {
            debug!("erasing file placed on tmpfs -- {:?}", p);
            Ok(utils::fs::rm(p).map(|_| true)?)
        } else {
            Ok(false)
        }
    }

    fn handle_specialfs<P: AsRef<Path>>(&self, p: P) -> Result<bool> {
        let p = p.as_ref();
        if self.mnt_utils.is_special_mount(p)? && !self.mnt_utils.is_tmpfs(p)? {
            warn!("removing special file system path TODO");
            Ok(utils::fs::rm(p).map(|_| true)?)
        } else {
            Ok(false)
        }
    }

    fn calc_hash<H: Hash>(h: &H) -> u64 {
        let mut hasher = DefaultHasher::new();
        h.hash(&mut hasher);
        hasher.finish()
    }
}

impl Default for RemovalImpl {
    fn default() -> Self {
        Self::new()
    }
}

impl Removal for RemovalImpl {
    fn backup<P: AsRef<Path>>(&mut self, paths: &[P]) -> Vec<Result<BackHandle>> {
        paths.iter().map(|p| self.backup_single_path(p)).collect()
    }

    fn restore_by_id(&mut self, ids: &[Id]) -> Vec<Result<()>> {
        ids.iter()
            .map(|id| self.restore_single_id(id.get_handle()))
            .collect()
    }

    fn erase_by_id(&mut self, ids: &[Id]) -> Vec<Result<()>> {
        ids.iter()
            .map(|id| self.erase_single_id(id.get_handle()))
            .collect()
    }

    fn restore_by_handle(&mut self, ids: &[u64]) -> Vec<Result<()>> {
        ids.iter().map(|id| self.restore_single_id(*id)).collect()
    }

    fn erase_by_handle(&mut self, ids: &[u64]) -> Vec<Result<()>> {
        ids.iter().map(|id| self.erase_single_id(*id)).collect()
    }

    fn list_backups(&self) -> Result<Vec<&Meta>> {
        Ok(self
            .mpath_to_meta
            .values()
            .flat_map(|vals| vals.values())
            .map(|val| val.as_ref())
            .collect())
    }
}
