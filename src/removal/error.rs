use crate::{config, metadata, utils};

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    Io(std::io::Error),
    Metadata(metadata::error::Error),
    Config(config::error::Error),
    Utils(utils::error::Error),
    Missing,
    Exists,
    Invalid,
    Excluded,
    TimedOut,
}

impl From<utils::error::Error> for Error {
    fn from(e: utils::error::Error) -> Self {
        Error::Utils(e)
    }
}

impl From<config::error::Error> for Error {
    fn from(e: config::error::Error) -> Self {
        Error::Config(e)
    }
}

impl From<metadata::error::Error> for Error {
    fn from(e: metadata::error::Error) -> Self {
        Error::Metadata(e)
    }
}

impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Self {
        Error::Io(e)
    }
}
