mod concrete;
pub mod error;

use super::metadata::{Id, Meta};
use crate::metadata::Handle;
pub use concrete::RemovalImpl as Rmv;
use error::Result;
use std::path::Path;

pub fn create_removal() -> Rmv {
    Rmv::new()
}

pub type BackHandle = Handle;
pub trait Removal {
    fn backup<P: AsRef<Path>>(&mut self, paths: &[P]) -> Vec<Result<BackHandle>>;
    fn restore_by_id(&mut self, ids: &[Id]) -> Vec<Result<()>>;
    fn erase_by_id(&mut self, ids: &[Id]) -> Vec<Result<()>>;
    fn restore_by_handle(&mut self, ids: &[BackHandle]) -> Vec<Result<()>>;
    fn erase_by_handle(&mut self, ids: &[BackHandle]) -> Vec<Result<()>>;
    fn list_backups(&self) -> Result<Vec<&Meta>>;
}
