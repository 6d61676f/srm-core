use super::{removal, utils};

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    Io(std::io::Error),
    Utils(utils::error::Error),
    Removal(removal::error::Error),
}

impl From<utils::error::Error> for Error {
    fn from(e: utils::error::Error) -> Self {
        Error::Utils(e)
    }
}

impl From<removal::error::Error> for Error {
    fn from(e: removal::error::Error) -> Self {
        Error::Removal(e)
    }
}

impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Self {
        Error::Io(e)
    }
}
