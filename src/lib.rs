#[macro_use]
extern crate log;

#[macro_use]
extern crate serde;

extern crate directories;
extern crate globset;
extern crate path_absolutize;
extern crate rand;
extern crate serde_json;
extern crate sysinfo;
extern crate tempfile;
extern crate toml;

use std::path::Path;

pub mod config;
pub mod error;

use self::{error::Result, metadata::Meta, removal::Removal};
pub use metadata::Id;
pub use removal::BackHandle;

mod metadata;
mod removal;
mod utils;

#[cfg(test)]
pub mod test_exports;
#[cfg(test)]
mod tests;

pub struct SafeRm {
    rem: removal::Rmv,
}

impl SafeRm {
    pub fn new() -> SafeRm {
        SafeRm {
            rem: removal::create_removal(),
        }
    }

    pub fn backup<P: AsRef<Path>>(&mut self, paths: &[P]) -> Vec<Result<BackHandle>> {
        self.rem.backup(paths).into_iter().map(|e| Ok(e?)).collect()
    }
    pub fn restore_by_id(&mut self, ids: &[Id]) -> Vec<Result<()>> {
        self.rem
            .restore_by_id(ids)
            .into_iter()
            .map(|e| Ok(e?))
            .collect()
    }
    pub fn restore_by_handle(&mut self, ids: &[BackHandle]) -> Vec<Result<()>> {
        self.rem
            .restore_by_handle(ids)
            .into_iter()
            .map(|e| Ok(e?))
            .collect()
    }
    pub fn erase_by_id(&mut self, ids: &[Id]) -> Vec<Result<()>> {
        self.rem
            .erase_by_id(ids)
            .into_iter()
            .map(|e| Ok(e?))
            .collect()
    }
    pub fn erase_by_handle(&mut self, ids: &[BackHandle]) -> Vec<Result<()>> {
        self.rem
            .erase_by_handle(ids)
            .into_iter()
            .map(|e| Ok(e?))
            .collect()
    }
    pub fn list_backups(&self) -> Result<Vec<&Meta>> {
        Ok(self.rem.list_backups()?)
    }
}

impl Default for SafeRm {
    fn default() -> Self {
        Self::new()
    }
}
