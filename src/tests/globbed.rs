use super::utils::{Files, _create_back};

struct Cfg {
    c_path: String,
}

impl Cfg {
    pub fn new() -> Self {
        let path = format!("{:?}-cfg", std::thread::current().id());
        let mut cfg = crate::config::Cfg::new();

        let globs: Vec<String> = vec!["**/.cache/**"]
            .into_iter()
            .map(|val| val.to_string())
            .collect();

        cfg.set_globs(Some(globs));

        cfg.write_file(&path);

        Self { c_path: path }
    }

    pub fn path(&self) -> Option<String> {
        Some(self.c_path.clone())
    }
}
impl Drop for Cfg {
    fn drop(&mut self) {
        std::fs::remove_file(&self.c_path).ok();
    }
}

#[test]
fn create_back() {
    let _cfg = Cfg::new();

    let files = Files::new(Some("./.cache"), 1);

    let backs = _create_back(&files.get_some_files(), _cfg.path());

    assert_eq!(backs.len(), 0);
}
