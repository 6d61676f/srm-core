mod globbed;
mod invalid;
#[cfg(target_os = "linux")]
mod links;
#[cfg(target_os = "linux")]
mod tmp;
// TODO -- method to clean folder structure for a depth
mod utils;
mod valid;

// Error -- TODO
// 1 create, backup, delete db, restore
// 2 create, backup, change db perm, restore
// 3 create, backup, change db perm, erase
// 4 create, change perm, backup
