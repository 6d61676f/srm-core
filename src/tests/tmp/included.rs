use crate::{
    removal::BackHandle,
    tests::utils::{
        Files, _backup, _check_backup, _check_erase, _check_restore, _create_back, _erase_backups,
        _get_back_list, _restore_backups,
    },
};

struct Cfg {
    c_path: String,
}

impl Cfg {
    pub fn new() -> Self {
        let path = format!("{:?}-cfg", std::thread::current().id());
        let mut cfg = crate::config::Cfg::new();
        cfg.set_tmpfs(Some(false));
        cfg.write_file(&path);

        Self { c_path: path }
    }

    pub fn path(&self) -> Option<String> {
        Some(self.c_path.clone())
    }
}
impl Drop for Cfg {
    fn drop(&mut self) {
        std::fs::remove_file(&self.c_path).ok();
    }
}

#[test]
fn create_backup_simple() {
    let _cfg = Cfg::new();
    let files = Files::new(Some("/tmp"), 0);

    let backs = _create_back(&files.get_some_files(), _cfg.path());
    _check_backup(&backs, _cfg.path());
    _restore_backups(&backs, _cfg.path());
    _check_restore(&backs, _cfg.path());
}

#[test]
fn create_backup_erase_simple() {
    let _cfg = Cfg::new();

    let files = Files::new(Some("/tmp"), 0);

    let backs = _create_back(&files.get_some_files(), _cfg.path());
    _check_backup(&backs, _cfg.path());
    _erase_backups(&backs, _cfg.path());
    let list = _get_back_list(_cfg.path());
    list.into_iter().all(|b| !backs.contains(&b.get_handle()));
}

#[test]
fn create_backup_restore_back() {
    let _cfg = Cfg::new();

    let files = Files::new(Some("/tmp"), 0);

    let backs = _create_back(&files.get_some_files(), _cfg.path());
    _check_backup(&backs, _cfg.path());
    _restore_backups(&backs, _cfg.path());
    _check_restore(&backs, _cfg.path());

    //backup again
    let backs = _backup(&files.get_some_files(), _cfg.path());
    assert!(backs.iter().all(|b| b.is_ok()));
    let backs: Vec<BackHandle> = backs.into_iter().filter_map(|b| b.ok()).collect();

    _check_backup(&backs, _cfg.path());
    _restore_backups(&backs, _cfg.path());
    _check_restore(&backs, _cfg.path());
}

#[test]
fn create_backup_create_backup() {
    let _cfg = Cfg::new();

    let files = Files::new(Some("/tmp"), 0);

    let backs = _create_back(&files.get_some_files(), _cfg.path());
    _check_backup(&backs, _cfg.path());

    files.create_file();
    let backs2 = _create_back(&files.get_some_files(), _cfg.path());
    _check_backup(&backs2, _cfg.path());

    _restore_backups(&backs, _cfg.path());
    _check_restore(&backs, _cfg.path());

    files.delete_files();

    _restore_backups(&backs2, _cfg.path());
    _check_restore(&backs, _cfg.path());
}

#[test]
fn create_backup_erase_create_backup_restore() {
    let _cfg = Cfg::new();

    let files = Files::new(Some("/tmp"), 0);

    let backs = _create_back(&files.get_some_files(), _cfg.path());

    _check_backup(&backs, _cfg.path());

    _erase_backups(&backs, _cfg.path());
    _check_erase(&backs, _cfg.path());

    files.create_file();
    let backs = _create_back(&files.get_some_files(), _cfg.path());
    _check_backup(&backs, _cfg.path());

    _restore_backups(&backs, _cfg.path());
    _check_restore(&backs, _cfg.path());
}
