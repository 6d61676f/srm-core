use path_absolutize::Absolutize;

use crate::{
    error::{Error, Result},
    metadata::{Handle, Meta},
    removal::{self, BackHandle},
    test_exports::Custom,
    SafeRm,
};
use std::{
    fs::File,
    path::{Path, PathBuf},
};

extern crate rand;

pub struct Files {
    fls: Vec<String>,
    whence: Option<String>,
    depth: u8,
}

impl Files {
    pub fn new(whence: Option<&str>, depth: u8) -> Self {
        simple_logging::log_to(File::create("log").unwrap(), log::LevelFilter::Debug);

        let fs = Files::get_some_names(rand::random(), whence);

        let ret = Self {
            fls: fs.into_iter().collect(),
            whence: whence.map(|val| val.to_owned()),
            depth,
        };

        ret.create_file();
        ret
    }

    pub fn create_file(&self) {
        self.fls.iter().for_each(|p| {
            let p = PathBuf::from(p);
            p.parent()
                .iter()
                .for_each(|v| std::fs::create_dir_all(v).expect("could not create parent"));
            if p.extension().is_some() {
                std::fs::create_dir(&p).unwrap_or_else(|_| panic!("could not create file {:?}", p));
            } else {
                std::fs::File::create(&p).unwrap_or_else(|_| panic!("could not create file {:?}", p));
            }
        });
    }

    pub fn delete_file<P: AsRef<Path>>(p: P) {
        std::fs::remove_file(&p)
            .unwrap_or_else(|_| warn!("could not delete file {}", p.as_ref().to_string_lossy()));
    }

    pub fn get_some_files(&self) -> Vec<String> {
        self.fls
            .iter()
            .map(|file| {
                PathBuf::from(file)
                    .absolutize()
                    .unwrap_or_else(|_| panic!("could not create abs for {}", file))
                    .to_string_lossy()
                    .to_string()
            })
            .collect()
    }

    pub fn clean_files(&self) {
        if let Some(offset) = self.fls.first().and_then(|val| {
            let pb = PathBuf::from(val);
            pb.parent().map(|v| v.to_owned())
        }) {
            std::fs::remove_dir_all(&offset)
                .unwrap_or_else(|_| error!("could not remove dir {}", offset.to_string_lossy()));
        }
        // "./path", 1 -- should delete
        // "/tmp , 0 -- should not delete
        // "/tmp/a -- 1 just a

        if let Some(whence) = &self.whence {
            if self.depth > 0 {
                let mut pb = PathBuf::from(whence)
                    .absolutize()
                    .expect("could not absolutize whence")
                    .to_path_buf();

                for _ in 1..self.depth {
                    pb = pb.parent().expect("could not ascend whence").to_path_buf();
                }

                info!("cleaning {}", pb.to_string_lossy());

                std::fs::remove_dir_all(&pb)
                    .unwrap_or_else(|_| error!("could not remove dir {:?}", pb));
            }
        }
    }

    pub fn delete_files(&self) {
        self.fls.iter().for_each(|f| {
            let pb = PathBuf::from(f);
            if pb.is_dir() {
                std::fs::remove_dir(pb).unwrap_or_else(|_| error!("could not remove dir {}", f));
            } else if pb.is_file() {
                std::fs::remove_file(pb).unwrap_or_else(|_| error!("could not remove file {}", f));
            }
        })
    }

    pub fn get_some_names(id: u32, whence: Option<&str>) -> Vec<String> {
        let whence = whence.unwrap_or("./");

        let dir = PathBuf::from(format!(
            "{}/{:?}-{}",
            whence,
            std::thread::current().id(),
            id,
        ));

        let names = vec!["aba", "b", "c", "d", "e"];

        let mut files: Vec<String> = names
            .iter()
            .map(|file| format!("{}/{}", dir.to_string_lossy(), file))
            .collect();

        files.extend(
            names
                .iter()
                .map(|file| format!("{}/{}.dir", dir.to_string_lossy(), file)),
        );

        files
    }
}
impl Drop for Files {
    fn drop(&mut self) {
        self.clean_files()
    }
}

pub fn _restore(backs: &[Handle], cfg: Option<String>) -> Vec<Result<()>> {
    SafeRm::new_custom(cfg).restore_by_handle(backs)
}

pub fn _erase(handles: &[Handle], cfg: Option<String>) -> Vec<Result<()>> {
    SafeRm::new_custom(cfg).erase_by_handle(handles)
}

pub fn _backup(paths: &[String], cfg: Option<String>) -> Vec<Result<BackHandle>> {
    SafeRm::new_custom(cfg).backup(paths)
}

pub fn _get_back_list(cfg: Option<String>) -> Vec<Meta> {
    SafeRm::new_custom(cfg)
        .list_backups()
        .unwrap_or_default()
        .into_iter()
        .cloned()
        .collect()
}

pub fn _create_back(paths: &[String], cfg: Option<String>) -> Vec<BackHandle> {
    let backs = _backup(paths, cfg);

    assert!(
        backs.iter().all(|rez| match rez {
            Ok(_) => true,
            Err(Error::Removal(removal::error::Error::Excluded)) => true,
            Err(_) => false,
        }),
        "{:?}",
        backs
    );

    backs
        .iter()
        .filter_map(|f| f.as_ref().ok())
        .cloned()
        .collect()
}

pub fn _check_backup(backs: &[BackHandle], cfg: Option<String>) {
    let list = _get_back_list(cfg);
    assert!(list.len() >= backs.len());

    let mut count = 0;

    for b in list {
        let handle = b.get_handle();
        if backs.iter().any(|elem| *elem == handle) {
            count += 1;
        }
    }

    assert_eq!(count, backs.len());
}

pub fn _restore_backups(backs: &[BackHandle], cfg: Option<String>) {
    let rez = _restore(backs, cfg);
    assert_eq!(rez.len(), backs.len());
    assert!(
        rez.iter().all(|r| r.is_ok()),
        "restore has failed {:?}",
        rez
    );
}

pub fn _check_restore(backs: &[BackHandle], cfg: Option<String>) {
    let list = _get_back_list(cfg);

    list.into_iter().for_each(|l| {
        let h = l.get_handle();
        backs.iter().for_each(|b| {
            assert_ne!(h, *b, "element {:?} has not been restored", l);
        });
    });
}

pub fn _erase_backups(backs: &[BackHandle], cfg: Option<String>) {
    let rez = _erase(backs, cfg);
    assert_eq!(rez.len(), backs.len());
    assert!(rez.iter().all(|r| r.is_ok()), "erase failed {:?}", rez);
}

pub fn _check_erase(backs: &[BackHandle], cfg: Option<String>) {
    let list = _get_back_list(cfg);
    for b in list.iter() {
        let handle = b.get_handle();
        backs.iter().for_each(|elem| {
            assert!(
                *elem != handle,
                "{:?} element {:?} has not been erased {:?} {} {}",
                std::thread::current().id(),
                b,
                list,
                elem,
                handle
            )
        });
    }
}
