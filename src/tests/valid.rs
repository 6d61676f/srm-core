// A. Correct

// 1. Create files, backup file
// 2. Create files, backup file, restore file
// 3. Create, backup, erase, clean
// 4. create, backup, restore, backup
// 5. create, backup, create, backup
// 6. create, backup, erase, create, backup, restore
use super::utils::{
    Files, _backup, _check_backup, _check_erase, _check_restore, _create_back, _erase_backups,
    _get_back_list, _restore_backups,
};
use crate::removal::BackHandle;

#[test]
fn create_back() {
    let files = Files::new(None, 0);

    let backs = _create_back(&files.get_some_files(), None);
    _check_backup(&backs, None);
    _restore_backups(&backs, None);
    _check_restore(&backs, None);
}

#[test]
fn create_back_restore() {
    create_back();
}

#[test]
fn simple() {
    let files = Files::new(None, 0);

    let backs = _create_back(&files.get_some_files(), None);
    _check_backup(&backs, None);
    _restore_backups(&backs, None);
    _check_restore(&backs, None);
}

#[test]
fn create_backup_erase() {
    let files = Files::new(None, 0);

    let backs = _create_back(&files.get_some_files(), None);
    _check_backup(&backs, None);
    _erase_backups(&backs, None);
    let list = _get_back_list(None);
    list.into_iter().all(|b| !backs.contains(&b.get_handle()));
}

#[test]
fn create_backup_restore_back() {
    let files = Files::new(None, 0);

    let backs = _create_back(&files.get_some_files(), None);
    _check_backup(&backs, None);
    _restore_backups(&backs, None);
    _check_restore(&backs, None);

    //backup again
    let backs = _backup(&files.get_some_files(), None);
    assert!(backs.iter().all(|b| b.is_ok()));
    let backs: Vec<BackHandle> = backs.into_iter().filter_map(|b| b.ok()).collect();

    _check_backup(&backs, None);
    _restore_backups(&backs, None);
    _check_restore(&backs, None);
}

#[test]
fn create_backup_create_backup() {
    let files = Files::new(None, 0);

    let backs = _create_back(&files.get_some_files(), None);
    _check_backup(&backs, None);

    files.create_file();
    let backs2 = _create_back(&files.get_some_files(), None);
    _check_backup(&backs2, None);

    _restore_backups(&backs, None);
    _check_restore(&backs, None);

    files.delete_files();

    _restore_backups(&backs2, None);
    _check_restore(&backs, None);
}

#[test]
fn create_backup_erase_create_backup_restore() {
    let files = Files::new(None, 0);

    let backs = _create_back(&files.get_some_files(), None);

    _check_backup(&backs, None);

    _erase_backups(&backs, None);
    _check_erase(&backs, None);

    files.create_file();
    let backs = _create_back(&files.get_some_files(), None);
    _check_backup(&backs, None);

    _restore_backups(&backs, None);
    _check_restore(&backs, None);
}
