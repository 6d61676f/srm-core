// B. Invalid

// 1 Create, backup, backup
// 2 Create, backup, restore, restore
// 3 Create, backup, restore, erase
// 4 Create, backup, erase, backup
// 5 Create, backup, erase, restore
// 6 Create, backup, erase, erase
// 7 create, backup, create, restore
use super::utils::{
    Files, _backup, _check_backup, _check_erase, _check_restore, _create_back, _erase,
    _erase_backups, _restore, _restore_backups,
};

#[test]
fn create_backup_backup() {
    let files = Files::new(None, 0);

    let backs = _create_back(&files.get_some_files(), None);
    _check_backup(&backs, None);

    let backs2 = _backup(&files.get_some_files(), None);
    assert!(
        backs2.iter().all(|b| b.is_err()),
        "backup of missing file {:?}",
        backs
    );

    _restore_backups(&backs, None);
    _check_restore(&backs, None);
}

#[test]
fn create_backup_restore_restore() {
    let files = Files::new(None, 0);

    let backs = _create_back(&files.get_some_files(), None);
    _check_backup(&backs, None);

    _restore_backups(&backs, None);
    _check_restore(&backs, None);

    let restored = _restore(&backs, None);
    assert!(
        restored.iter().all(|r| r.is_err()),
        "double restore {:?}",
        restored
    );
}

#[test]
fn create_backup_restore_erase() {
    let files = Files::new(None, 0);

    let backs = _create_back(&files.get_some_files(), None);
    _check_backup(&backs, None);
    _restore_backups(&backs, None);
    _check_restore(&backs, None);

    let erased = _erase(&backs, None);

    assert!(
        erased.iter().all(|e| e.is_err()),
        "erase of restored {:?}",
        erased
    );
}

#[test]
fn create_backup_erase_backup() {
    let files = Files::new(None, 0);

    let backs = _create_back(&files.get_some_files(), None);
    _check_backup(&backs, None);

    _erase_backups(&backs, None);
    _check_erase(&backs, None);

    let backed = _backup(&files.get_some_files(), None);
    assert!(
        backed.iter().all(|b| b.is_err()),
        "backup of missing file {:?}",
        backed
    );
}

#[test]
fn create_backup_erase_restore() {
    let files = Files::new(None, 0);

    let backs = _create_back(&files.get_some_files(), None);
    _check_backup(&backs, None);

    _erase_backups(&backs, None);
    _check_erase(&backs, None);

    let restored = _restore(&backs, None);

    assert!(
        restored.iter().all(|b| b.is_err()),
        "restored missing file {:?}",
        restored
    );
}

#[test]
fn create_backup_erase_erase() {
    let files = Files::new(None, 0);

    let backs = _create_back(&files.get_some_files(), None);
    _check_backup(&backs, None);

    _erase_backups(&backs, None);
    _check_erase(&backs, None);

    let erased = _erase(&backs, None);

    assert!(
        erased.iter().all(|e| e.is_err()),
        "erase of restored {:?}",
        erased
    );
}

#[test]
fn create_backup_create_restore() {
    let files = Files::new(None, 0);

    let backs = _create_back(&files.get_some_files(), None);
    _check_backup(&backs, None);

    files.create_file();

    let restored = _restore(&backs, None);

    assert!(
        restored.iter().all(|b| b.is_err()),
        "restored would overwrite file {:?}",
        restored
    );

    files.delete_files();

    _restore_backups(&backs, None);
    _check_restore(&backs, None);
}
