use crate::{
    test_exports::fs::Mount,
    tests::utils::{Files, _check_backup, _check_erase, _create_back, _erase_backups},
};
use path_absolutize::Absolutize;
use std::path::PathBuf;

struct Cfg {
    c_path: String,
}

impl Cfg {
    fn new() -> Self {
        let path = format!("{:?}-cfg", std::thread::current().id());
        let mut cfg = crate::config::Cfg::new();
        cfg.set_tmpfs(Some(true));
        cfg.write_file(&path);

        Self { c_path: path }
    }

    fn path(&self) -> Option<String> {
        Some(self.c_path.clone())
    }
}

impl Drop for Cfg {
    fn drop(&mut self) {
        std::fs::remove_file(&self.c_path).ok();
    }
}

// actual scenario
// /opt/games -- A
// /home -- B
// /tmp -- C

// create /tmp/user
// create /home/user/cacat
// create /opt/games/some_folder

// link /tmp/user /home/user/cacat/tmp
// create /home/user/cacat/tmp/links

// link /home/user/cacat /opt/games/some_folder/cacat
// link /opt/games/some_folder /home/user/cacat/tmp/links/some_folder

// touch /home/user/cacat/cacat
// touch /opt/games/some_folder/some_file

// Delete /home/user/cacat/tmp/links/some_folder/some_file -- backsup on A
// Delete /home/user/cacat/tmp/links/some_folder/cacat/cacat -- backups on B
// Delete /home/user/cacat/tmp/links/some_folder/cacat -- backups on A
// Delete /home/user/cacat/tmp/links/some_folder -- tmp excluded erased
// Delete /home/user/cacat/tmp/links -- tmp excluded erased
#[test]
fn shady() {
    let rand: u32 = rand::random();
    let home = env!("HOME");
    let tmp = "/tmp"; // MUST BE TMPFS -- TODO maybe check somehow

    let cross = "/opt/games"; // MUST BE ON A DIFFERENT MOUNT POINT THAN HOME -- TODO maybe enforce somehow

    // some setup sanity
    {
        assert!(!home.is_empty());

        let mut mount = Mount::new();
        mount.refresh();
        assert!(mount.is_tmpfs(tmp).expect("could not determine tmpfs"));

        let home_mount = mount
            .get_mount_from_path(home)
            .expect("could not determine mount for home");

        let cross_mount = mount
            .get_mount_from_path(cross)
            .expect("could not determine mount for cross");

        assert_ne!(home_mount, cross_mount);
    }

    let tmp_rand = format!("{}/{}", tmp, rand);
    let home_rand = format!("{}/whatever-home-{}", home, rand);
    let cross_rand = format!("{}/whatever-cross-{}", cross, rand);

    let _tmp = Files::new(Some(&tmp_rand), 1);
    let _home = Files::new(Some(&home_rand), 1);
    let _cross = Files::new(Some(&cross_rand), 1);

    let home_tmp = format!("{}/tmp", home_rand);

    std::os::unix::fs::symlink(&tmp_rand, &home_tmp).expect("could not link tmp to home/tmp");

    let cross_home = format!("{}/home", cross_rand);
    std::os::unix::fs::symlink(&home_rand, &cross_home)
        .expect("could not link home_rand to cross_home");

    let home_tmp_links = format!("{}/links", home_tmp);
    let _home_tmp = Files::new(Some(&home_tmp_links), 0);
    let home_tmp_links_cross = format!("{}/cross", home_tmp_links);

    std::os::unix::fs::symlink(&cross_rand, &home_tmp_links_cross)
        .expect("could not link cross_rand to cross_tmp_links_cross");

    // this was only the setup ---- and it's fucked up
    // note, compared to the original scenario we have a possible link loop

    let _cfg = Cfg::new();
    // A delete $HOME/whatever-home/tmp/links/cross/file -- backup to cross -- file
    {
        // we need paths relative to home
        let files: Vec<String> = _cross
            .get_some_files()
            .into_iter()
            .map(|path| {
                let path = PathBuf::from(path);
                let file = path
                    .file_name()
                    .expect("could not get file_name for cross file")
                    .to_string_lossy();

                let parent = path
                    .parent()
                    .expect("could not get parent for cross file")
                    .file_name()
                    .expect("could not get file_name of parent for cross file")
                    .to_string_lossy();

                format!("{}/{}/{}", home_tmp_links_cross, parent, file)
            })
            .collect();

        assert!(files.iter().all(|f| PathBuf::from(f).exists()));

        let backs = _create_back(&files, _cfg.path());
        _check_backup(&backs, _cfg.path());
        _erase_backups(&backs, _cfg.path());
        _check_erase(&backs, _cfg.path());
    }

    // B delete $HOME/whatever-home/tmp/links/cross/home/file -- backup in home --file
    {
        // we need paths relative to home
        let files: Vec<String> = _home
            .get_some_files()
            .into_iter()
            .map(|path| {
                let path = PathBuf::from(path);
                let file = path
                    .file_name()
                    .expect("could not get file name for home file")
                    .to_string_lossy();

                let parent = path
                    .parent()
                    .expect("could not get parent for home file")
                    .file_name()
                    .expect("could not get file_name of parent for home file")
                    .to_string_lossy();

                format!(
                    "{}/{}/{}/{}",
                    home_tmp_links_cross,
                    PathBuf::from(&cross_home)
                        .file_name()
                        .expect("could not get file_name for cross_home")
                        .to_string_lossy(),
                    parent,
                    file
                )
            })
            .collect();

        assert!(files.iter().all(|f| PathBuf::from(f).exists()));

        let backs = _create_back(&files, _cfg.path());
        _check_backup(&backs, _cfg.path());
        _erase_backups(&backs, _cfg.path());
        _check_erase(&backs, _cfg.path());
    }

    // C delete $HOME/whatever-home/tmp/links/cross/home -- backup on cross -- link
    {
        let link = format!(
            "{}/{}",
            home_tmp_links_cross,
            PathBuf::from(&cross_home)
                .file_name()
                .expect("wtf4")
                .to_string_lossy()
        );

        assert!(PathBuf::from(&link).exists());

        let backs = _create_back(&[link], _cfg.path());
        _check_backup(&backs, _cfg.path());
        _erase_backups(&backs, _cfg.path());
        _check_erase(&backs, _cfg.path());
    }

    // D delete $HOME/whatever-home/tmp/links/cross -- tmp excluded erase
    {
        let backs = _create_back(&[home_tmp_links_cross], _cfg.path());
        assert!(backs.is_empty());
    }

    // E delete $HOME/whatever-home/tmp/links -- tmp excluded erase
    {
        let backs = _create_back(&[home_tmp_links], _cfg.path());
        assert!(backs.is_empty());
    }
}

#[test]
fn create_back_cross_sym() {
    let _cfg = Cfg::new();

    // create folder on current device
    let names = Files::new(
        Some(&format!("./linked_in_tmp-{}", rand::random::<usize>())),
        1,
    );
    let files = names.get_some_files();

    assert!(!files.is_empty());

    let mut src = PathBuf::from(files.get(0).unwrap());
    src.set_extension("dir");
    src = src
        .absolutize()
        .unwrap_or_else(|_| panic!("could not absolutize {}", src.to_string_lossy()))
        .to_path_buf();

    let mut target = PathBuf::from("/tmp");
    target.push(rand::random::<usize>().to_string());

    // create files in it
    let actual_files = Files::new(Some(&src.to_string_lossy()), 1);

    // link it to some tmp folder
    std::os::unix::fs::symlink(src, &target).expect("symlink to tmp has failed");

    {
        let mut m = Mount::new();
        m.refresh();
        assert!(m.is_tmpfs(&target).expect("could not determine if tmpfs"));
    }

    // expect them to get backed-up
    let backs = _create_back(&actual_files.get_some_files(), _cfg.path());
    _check_backup(&backs, _cfg.path());
    _erase_backups(&backs, _cfg.path());
    _check_erase(&backs, _cfg.path());
    Files::delete_file(target);
}

#[test]
fn backup_cross_sym() {
    let _cfg = Cfg::new();

    // create folder on current device
    let names = Files::new(
        Some(&format!("./linked_in_tmp-{}", rand::random::<usize>())),
        1,
    );
    let files = names.get_some_files();

    assert!(!files.is_empty());

    let mut src = PathBuf::from(files.get(0).unwrap());
    src.set_extension("dir");
    src = src
        .absolutize()
        .unwrap_or_else(|_| panic!("could not absolutize {}", src.to_string_lossy()))
        .to_path_buf();

    let target = PathBuf::from("/tmp")
        .join(rand::random::<usize>().to_string())
        .absolutize()
        .expect("could not absolutize target")
        .to_string_lossy()
        .to_string();

    // create files in it
    let _actual_files = Files::new(Some(&src.to_string_lossy()), 1);

    // link it to some tmp folder
    std::os::unix::fs::symlink(src, &target).expect("symlink to tmp has failed");

    {
        let mut m = Mount::new();
        m.refresh();
        assert!(m.is_tmpfs(&target).expect("could not determine if tmpfs"));
    }

    // expect it to get deleted
    let backs = _create_back(&[target], _cfg.path());
    assert!(backs.is_empty());
}

#[test]
fn simple_cross() {
    let rand: u32 = rand::random();
    let home = env!("HOME");
    let cross = "/opt/games";

    {
        assert!(!home.is_empty());

        let mut mount = Mount::new();
        mount.refresh();

        let home_mount = mount
            .get_mount_from_path(home)
            .expect("could not determine mount for home");

        let cross_mount = mount
            .get_mount_from_path(cross)
            .expect("could not determine mount for cross");

        assert_ne!(home_mount, cross_mount);
    }

    let home_rand = format!("{}/whatever-home-{}", home, rand);
    let cross_rand = format!("{}/whatever-cross-{}", cross, rand);

    let _home = Files::new(Some(&home_rand), 1);

    std::os::unix::fs::symlink(&home_rand, &cross_rand)
        .expect("could not link home_rand to cross_rand");

    let backs = _create_back(&[cross_rand], None);
    _check_backup(&backs, None);
    _erase_backups(&backs, None);
    _check_erase(&backs, None);
}
