use crate::{removal::Rmv, SafeRm};
use std::path::Path;

pub use super::utils::fs;

pub trait Custom {
    fn new_custom<P: AsRef<Path>>(p: Option<P>) -> Self;
}

pub fn create_removal_custom<P: AsRef<Path>>(p: Option<P>) -> Rmv {
    if let Some(p) = p {
        Rmv::new_custom_cfg(p)
    } else {
        Rmv::new()
    }
}

impl Custom for SafeRm {
    fn new_custom<P: AsRef<Path>>(p: Option<P>) -> Self {
        SafeRm {
            rem: create_removal_custom(p),
        }
    }
}
