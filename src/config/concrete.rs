use serde::{
    de::Visitor,
    ser::{Serialize, SerializeSeq},
    Deserialize,
};
use std::{
    cell::RefCell,
    collections::{hash_map::DefaultHasher, HashMap},
    fs::{self, OpenOptions},
    hash::{Hash, Hasher},
    path::{Path, PathBuf},
    rc::Rc,
};

use directories::BaseDirs;
use tempfile::tempfile_in;

use crate::{
    config::{
        error::{Error, MissingItem, Result},
        Config,
    },
    utils::fs::Mount,
};

#[derive(Debug, Clone, Default)]
struct GlobSerde {
    path: Vec<Rc<String>>,
}

impl Serialize for GlobSerde {
    fn serialize<S>(&self, serializer: S) -> core::result::Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut seq = serializer.serialize_seq(Some(self.path.len()))?;
        for e in self.path.iter() {
            seq.serialize_element(e.as_ref())?;
        }
        seq.end()
    }
}

struct GlobSerdeVisitor;

impl<'de> Visitor<'de> for GlobSerdeVisitor {
    type Value = GlobSerde;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        formatter.write_str("a vector of strings representing globs")
    }

    fn visit_seq<A>(self, mut seq: A) -> core::result::Result<Self::Value, A::Error>
    where
        A: serde::de::SeqAccess<'de>,
    {
        let mut vals = Vec::with_capacity(seq.size_hint().unwrap_or(0));

        while let Some(val) = seq.next_element()? {
            vals.push(Rc::new(val));
        }

        Ok(GlobSerde { path: vals })
    }
}

impl<'de> Deserialize<'de> for GlobSerde {
    fn deserialize<D>(deserializer: D) -> core::result::Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        deserializer.deserialize_seq(GlobSerdeVisitor)
    }
}

#[derive(Deserialize, Serialize, Debug, Default)]
struct CfgFile {
    count: Option<usize>,
    home: Option<PathBuf>,
    meta: Option<PathBuf>,
    back: Option<PathBuf>,
    tmpfs: Option<bool>,
    pids: Option<Vec<usize>>,
    gids: Option<Vec<usize>>,
    uids: Option<Vec<usize>>,
    ex_paths: Option<GlobSerde>,
}

impl CfgFile {
    pub fn load() -> CfgFile {
        CfgFile::path()
            .and_then(CfgFile::load_from)
            .unwrap_or_default()
    }

    fn load_from<P: AsRef<Path>>(p: P) -> Result<CfgFile> {
        Ok(toml::from_str::<CfgFile>(&fs::read_to_string(p)?)?)
    }

    #[cfg(test)]
    pub fn load_custom<P: AsRef<Path>>(p: P) -> Result<CfgFile> {
        CfgFile::load_from(p)
    }

    pub fn path() -> Result<PathBuf> {
        const PKG_NAME: &str = env!("CARGO_PKG_NAME");

        let cfg_dir = directories::ProjectDirs::from("com", "meh", PKG_NAME)
            .ok_or(Error::Missing(MissingItem::Home))?
            .config_dir()
            .to_path_buf();

        Ok(cfg_dir.join("config"))
    }
}

type PathHash = u64;

fn calc_hash<H: Hash>(h: &H) -> PathHash {
    let mut hasher = DefaultHasher::new();
    h.hash(&mut hasher);
    hasher.finish()
}

type PCache = RefCell<HashMap<PathHash, Rc<PathBuf>>>;

pub struct CfgImpl {
    file: CfgFile,
    mount_util: Mount,
    // a cache for the values of the next three caches
    // hash(result from det meta/back/home) -> value
    path_cache: PCache,
    // hash(user path) -> project home
    homes: PCache,
    // hash(user path) -> meta file
    metas: PCache,
    // hash(user path) -> backup path
    backs: PCache,
}

impl Default for CfgImpl {
    fn default() -> Self {
        Self::new()
    }
}

impl CfgImpl {
    pub fn new() -> CfgImpl {
        let mut mutils = Mount::new();
        mutils.refresh();

        CfgImpl {
            file: CfgFile::load(),
            mount_util: mutils,
            path_cache: RefCell::new(HashMap::new()),
            homes: RefCell::new(HashMap::new()),
            metas: RefCell::new(HashMap::new()),
            backs: RefCell::new(HashMap::new()),
        }
    }

    #[cfg(test)]
    pub fn new_custom<P: AsRef<Path>>(p: P) -> CfgImpl {
        let mut cfg = CfgImpl::new();
        cfg.file = CfgFile::load_custom(p).unwrap();
        cfg
    }

    #[cfg(test)]
    pub fn set_tmpfs(&mut self, flag: Option<bool>) {
        self.file.tmpfs = flag;
    }

    #[cfg(test)]
    pub fn set_count(&mut self, count: Option<usize>) {
        self.file.count = count
    }

    #[cfg(test)]
    pub fn set_globs(&mut self, globs: Option<Vec<String>>) {
        let glob = globs.map(|vals| GlobSerde {
            path: vals.into_iter().map(Rc::new).collect(),
        });

        self.file.ex_paths = glob;
    }

    #[cfg(test)]
    pub fn write_file<P: AsRef<Path>>(&self, p: P) {
        let p = p.as_ref();

        if let Some(parent) = p.parent() {
            std::fs::create_dir_all(parent).unwrap();
        }

        std::fs::write(p, toml::to_string(&self.file).unwrap()).unwrap()
    }

    fn can_access_dir<P: AsRef<Path>>(p: P) -> bool {
        fs::create_dir_all(&p)
            .map_err(|err| {
                error!("access create failed {}", err);
                err
            })
            .and_then(|_| tempfile_in(&p))
            .map_err(|err| error!("access tmp failed {} {:?}", err, p.as_ref()))
            .is_ok()
    }

    fn can_access_file<P: AsRef<Path>>(p: P) -> bool {
        let p = p.as_ref();
        let mut ok = true;
        if let Some(parent) = p.parent() {
            ok &= fs::create_dir_all(parent).is_ok();
        }

        ok &= OpenOptions::new().create(true).write(true).open(p).is_ok();

        ok
    }

    fn srm_home<P: AsRef<Path>>(p: P, hidden: bool) -> Rc<PathBuf> {
        let dot = if hidden { "." } else { "" };
        let p = p.as_ref().to_path_buf();
        Rc::new(p.join(format!("{}{}", dot, env!("CARGO_PKG_NAME"))))
    }

    fn meta_file<P: AsRef<Path>>(p: P) -> Rc<PathBuf> {
        let p = p.as_ref().to_path_buf();
        Rc::new(p.join("meta"))
    }

    fn det_srm_home<P: AsRef<Path>>(&self, p: P) -> Result<Rc<PathBuf>> {
        let p = p.as_ref();
        //let p = CfgImpl::parent_canonical(p)?;
        let hash = calc_hash(&p);

        if let Some(cached) = self.homes.borrow().get(&hash) {
            return Ok(cached.clone());
        }

        debug!("trying to determine home project folder for {:?}", &p);

        // this is a hack to get project XDG_ stuff which should get another func
        let not_empty = if !p.as_os_str().is_empty() {
            let file_home = self
                .file
                .home
                .as_ref()
                .map(|home| Rc::new(home.to_path_buf()));

            let mount_home = self
                .mount_util
                .get_mount_from_path(p)
                .ok()
                .map(|home| CfgImpl::srm_home(home, true))
                .filter(|home| CfgImpl::can_access_dir(home.as_ref())); // we need to keep this filter here...

            file_home.or(mount_home)
        } else {
            None
        };

        let bd = BaseDirs::new();

        let xdg_data = bd
            .as_ref()
            .map(|base_dirs| CfgImpl::srm_home(base_dirs.data_dir(), false));

        let xdg_home = bd.map(|base_dirs| CfgImpl::srm_home(base_dirs.home_dir(), true));

        not_empty
            .or(xdg_data)
            .or(xdg_home)
            .filter(|dir| CfgImpl::can_access_dir(dir.as_ref()))
            .ok_or(Error::Missing(MissingItem::Home))
            .map(|path| {
                let cache = self
                    .path_cache
                    .borrow_mut()
                    .entry(calc_hash(path.as_ref()))
                    .or_insert_with(|| path.clone())
                    .clone();

                debug!("homes cache {} -> {}", hash, cache.to_string_lossy());
                self.homes.borrow_mut().insert(hash, cache);
                path
            })
    }

    // i'm still skeptical...TODO
    fn parent_canonical<P: AsRef<Path>>(p: P) -> Result<PathBuf> {
        let ppath = if let Some(parent) = p.as_ref().parent() {
            parent
        } else {
            p.as_ref()
        };

        let abs_can = ppath.canonicalize()?;

        debug!(
            "parent canonical path for {} is {}",
            p.as_ref().to_string_lossy(),
            abs_can.to_string_lossy()
        );

        Ok(abs_can)
    }

    // TODO -- meta and back are more or less the same so we should join them
    // also we should extract the snippet that computes the correct home
    fn det_meta<P: AsRef<Path>>(&self, p: P) -> Result<Rc<PathBuf>> {
        let p = CfgImpl::parent_canonical(p)?;
        let hash = calc_hash(&p);

        if let Some(cached) = self.metas.borrow().get(&hash) {
            return Ok(cached.clone());
        }

        let file_meta = self.file.meta.as_ref().map(|meta| Rc::new(meta.clone()));

        let meta = self.det_srm_home(&p).ok().map(|home| {
            let mut home = (*home).clone();
            home.push(calc_hash(&p).to_string());
            CfgImpl::meta_file(home)
        });

        file_meta
            .or(meta)
            .filter(|dir| CfgImpl::can_access_file(dir.as_ref()))
            .ok_or(Error::Missing(MissingItem::Meta))
            .map(|path| {
                let cache = self
                    .path_cache
                    .borrow_mut()
                    .entry(calc_hash(path.as_ref()))
                    .or_insert_with(|| path.clone())
                    .clone();

                debug!("meta cache {} -> {}", hash, cache.to_string_lossy());
                self.metas.borrow_mut().insert(hash, cache);
                path
            })
    }

    fn det_back<P: AsRef<Path>>(&self, p: P) -> Result<Rc<PathBuf>> {
        let p = CfgImpl::parent_canonical(p)?;
        let hash = calc_hash(&p);

        if let Some(cached) = self.backs.borrow().get(&hash) {
            return Ok(cached.clone());
        }

        let file_back = self.file.back.as_ref().map(|back| Rc::new(back.clone()));

        let back = self.det_srm_home(&p).ok().map(|home| {
            let mut home = (*home).clone();
            home.push(calc_hash(&p).to_string());
            Rc::new(home)
        });

        file_back
            .or(back)
            .filter(|dir| CfgImpl::can_access_dir(dir.as_ref()))
            .ok_or(Error::Missing(MissingItem::Backup))
            .map(|path| {
                let cache = self
                    .path_cache
                    .borrow_mut()
                    .entry(calc_hash(path.as_ref()))
                    .or_insert_with(|| path.clone())
                    .clone();

                debug!("back cache {} -> {}", hash, cache.to_string_lossy());
                self.backs.borrow_mut().insert(hash, cache);
                path
            })
    }
}

impl Config for CfgImpl {
    fn count(&self) -> usize {
        self.file.count.unwrap_or(10)
    }

    fn back_path<P: AsRef<Path>>(&self, p: P) -> Result<Rc<PathBuf>> {
        self.det_back(p)
    }

    fn meta_path<P: AsRef<Path>>(&self, p: P) -> Result<Rc<PathBuf>> {
        self.det_meta(p)
    }

    fn get_all_meta_paths(&self) -> Vec<Rc<PathBuf>> {
        let mut mps: Vec<Rc<PathBuf>> = self
            .mount_util
            .get_mount_points()
            .into_iter()
            .map(|p| CfgImpl::srm_home(p, true))
            .collect::<Vec<Rc<PathBuf>>>();

        if let Ok(home) = self.det_srm_home("") {
            mps.push(home);
        }

        mps.into_iter()
            .filter_map(|path| fs::read_dir(&*path).ok())
            .fold(vec![], |mut acc, readdir| {
                acc.extend(
                    readdir
                        .filter_map(|entry| entry.ok())
                        .map(|entry| entry.path())
                        .filter(|entry| entry.is_dir())
                        .map(CfgImpl::meta_file)
                        .filter(|meta| meta.exists()),
                );
                acc
            })
    }

    fn exclude_tmpfs(&self) -> bool {
        self.file.tmpfs.unwrap_or_default()
    }

    fn get_sample() -> Result<String> {
        let cfg = CfgImpl::new();
        Ok(toml::to_string(&cfg.file)?)
    }

    fn config_path() -> Result<PathBuf> {
        CfgFile::path()
    }

    fn excl_paths(&self) -> Vec<Rc<String>> {
        self.file.ex_paths.clone().unwrap_or_default().path
    }
}
