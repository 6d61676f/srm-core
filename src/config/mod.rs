mod concrete;
pub mod error;

use self::error::Result;
pub use concrete::CfgImpl as Cfg;
use std::{
    path::{Path, PathBuf},
    rc::Rc,
};

pub fn get_config() -> Cfg {
    concrete::CfgImpl::new()
}

pub trait Config {
    fn count(&self) -> usize;

    //given a file for potential removal we need to identify where its metadata and backup should be
    fn back_path<P: AsRef<Path>>(&self, p: P) -> Result<Rc<PathBuf>>;
    fn meta_path<P: AsRef<Path>>(&self, p: P) -> Result<Rc<PathBuf>>;

    fn get_all_meta_paths(&self) -> Vec<Rc<PathBuf>>;

    fn exclude_tmpfs(&self) -> bool {
        false
    }
    fn excl_pid(&self) -> Vec<usize> {
        Vec::new()
    }
    fn excl_gid(&self) -> Vec<usize> {
        Vec::new()
    }
    fn excl_uid(&self) -> Vec<usize> {
        Vec::new()
    }
    fn excl_paths(&self) -> Vec<Rc<String>> {
        Vec::new()
    }

    fn get_sample() -> Result<String>;

    fn config_path() -> Result<PathBuf>;
}
