pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    Io((std::io::ErrorKind, std::io::Error)),
    Missing,
    Invalid,
}

impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Self {
        Error::Io((e.kind(), e))
    }
}
