pub mod error;

pub mod fs {
    use super::error::{Error, Result};
    use {
        std::{
            cmp::Reverse,
            collections::{HashMap, HashSet},
            fs::OpenOptions,
            path::{Path, PathBuf},
            rc::Rc,
            sync::OnceLock,
        },
        sysinfo::Disks,
    };

    pub fn mv<P>(from: P, to: P) -> Result<()>
    where
        P: AsRef<Path>,
    {
        let from = from.as_ref();
        let to = to.as_ref();

        if Mount::is_symlink(from) || from.is_file() {
            Ok(std::fs::rename(from, to)?)
        } else {
            std::fs::create_dir_all(to)?;
            Ok(std::fs::rename(from, to)?)
        }
    }

    pub fn rm<P: AsRef<Path>>(p: P) -> Result<()> {
        if Mount::is_symlink(&p) || p.as_ref().is_file() {
            Ok(std::fs::remove_file(p)?)
        } else if p.as_ref().is_dir() {
            Ok(std::fs::remove_dir_all(p)?)
        } else {
            Err(Error::Invalid)
        }
    }

    #[derive(Debug, Clone)]
    struct ProcMounts {
        //block: String,
        //point: PathBuf,
        kind: String,
    }

    impl ProcMounts {
        //TODO -- have an enum on init and populate it once then
        pub fn is_tmpfs(&self) -> bool {
            self.kind.to_lowercase().eq("tmpfs") || self.kind.to_lowercase().eq("ramfs")
        }
    }

    static DISK_DATA: OnceLock<Disks> = OnceLock::new();

    pub struct Mount {
        disks: &'static Disks,
        mounts: HashSet<PathBuf>,
        specials: HashMap<PathBuf, ProcMounts>,
    }

    impl Default for Mount {
        fn default() -> Self {
            Self::new()
        }
    }

    impl Mount {
        pub fn new() -> Mount {
            Mount {
                disks: DISK_DATA.get_or_init(Disks::new_with_refreshed_list),
                mounts: HashSet::new(),
                specials: HashMap::new(),
            }
        }

        pub fn is_symlink<P: AsRef<Path>>(path: P) -> bool {
            path.as_ref().is_symlink()
            // .symlink_metadata()
            // .map(|meta| meta.file_type().is_symlink())
            // .unwrap_or_default()
        }

        pub fn get_mount_points(&self) -> Vec<PathBuf> {
            let mut dmp: Vec<PathBuf> = self.get_disk_mount_points().iter().cloned().collect();
            let mut smp = self.get_special_mounts().keys().cloned().collect();

            dmp.append(&mut smp);

            dmp
        }

        pub fn get_disk_mount_points(&self) -> &HashSet<PathBuf> {
            &self.mounts
        }

        // here paths must be absolute!
        pub fn get_mount_from_path<P: AsRef<Path>>(&self, path: P) -> Result<PathBuf> {
            let path = path.as_ref();
            let mut mps: Vec<PathBuf> = self.get_mount_points();
            mps.sort_by_key(|b| Reverse(b.as_os_str().len()));

            for p in mps {
                if path
                    .as_os_str()
                    .to_str()
                    .ok_or(Error::Missing)?
                    .contains(p.as_os_str().to_str().ok_or(Error::Missing)?)
                {
                    debug!(
                        "patch matched mount {} -> {}",
                        path.to_string_lossy(),
                        p.to_string_lossy()
                    );
                    return Ok(p);
                }
            }

            Err(Error::Missing)
        }

        pub fn refresh(&mut self) {
            self.refresh_mount_points();
            #[cfg(target_os = "linux")]
            self.refresh_specials();
        }

        fn refresh_mount_points(&mut self) {
            self.mounts = self
                .disks
                .iter()
                .map(|v| v.mount_point().to_path_buf())
                .collect();
        }

        #[cfg(target_os = "linux")]
        fn refresh_specials(&mut self) {
            let mut mounts =
                std::fs::read_to_string("/proc/mounts").map_or(HashMap::new(), |string| {
                    let mut _mounts = HashMap::new();

                    string.lines().for_each(|line| {
                        let elements: Vec<&str> = line.split_ascii_whitespace().collect();
                        if elements.len() < 2 {
                        } else {
                            _mounts.insert(
                                PathBuf::from(elements.get(1).unwrap()),
                                ProcMounts {
                                    kind: elements.get(2).unwrap().to_string(),
                                },
                            );
                        }
                    });

                    _mounts
                });

            for mnt in self.get_disk_mount_points() {
                mounts.remove(mnt);
            }
            self.specials = mounts;
        }

        // basically if it's anything besides regular mount paths
        // read /proc/mounts on linux and compare with `list_mpaths`
        fn get_special_mounts(&self) -> &HashMap<PathBuf, ProcMounts> {
            &self.specials
        }

        #[cfg(not(target_os = "linux"))]
        pub fn is_special_mount<P: AsRef<Path>>(&self, _: P) -> Result<bool> {
            warn!("is_special_mount not implemented besides linux");
            Ok(false)
        }
        #[cfg(target_os = "linux")]
        pub fn is_special_mount<P: AsRef<Path>>(&self, pth: P) -> Result<bool> {
            let pth = pth.as_ref();
            let meta = std::fs::symlink_metadata(pth)?;
            use std::os::linux::fs::MetadataExt;

            // man 7 inode
            let mode = meta.st_mode() & 0o170000;
            match mode {
                0o040000 | 0o100000 | 0o120000 => Ok(false),
                _ => {
                    warn!("user passed special file {:?}", pth);
                    Ok(true)
                }
            }
        }

        pub fn is_tmpfs<P: AsRef<Path>>(&self, pth: P) -> Result<bool> {
            let pth = if Mount::is_symlink(&pth) {
                pth.as_ref()
                    .parent()
                    .ok_or(Error::Missing)?
                    .canonicalize()?
            } else {
                pth.as_ref().canonicalize()?
            };

            for (k, v) in &self.specials {
                if v.is_tmpfs() && pth.starts_with(k) {
                    debug!("user passed tmpfs path {:?}", pth);
                    return Ok(true);
                }
            }
            Ok(false)
        }
    }

    pub struct FileLock {
        path: Rc<PathBuf>,
    }

    impl FileLock {
        pub fn try_new<P: AsRef<Path>>(path: P) -> Result<FileLock> {
            Ok(FileLock {
                path: Rc::new(path.as_ref().to_path_buf()),
            })
        }

        pub fn try_lock(&self) -> Result<LockHandle> {
            let _ = OpenOptions::new()
                .create_new(true)
                .write(true)
                .open(&*self.path)?;

            Ok(LockHandle {
                path: Rc::clone(&self.path),
            })
        }
    }

    pub struct LockHandle {
        path: Rc<PathBuf>,
    }

    impl Drop for LockHandle {
        fn drop(&mut self) {
            if let Err(e) = std::fs::remove_file(&*self.path) {
                error!("could not remove lock for {:?} {:?}", self.path, e);
            }
        }
    }
}
